if FirstLoad then
	TempGridThread = false
end
MapVar("TempGridMesh", false)
MapVar("TempGridText", false)

function OnMsg.LoadGame()
	ShowTempGrid()
end

function ToggleTempGrid(show)
	if show == nil then
		show = not IsValidThread(TempGridThread)
	end
	DeleteThread(TempGridThread)
	ShowTempGrid()
	if not show then return end
	TempGridThread = CreateMapRealTimeThread(function()
		local last_time = 0
		local last_pos = point()
		while true do
			local time = RealTime()
			local pos = terrain.FindPassable(GetCursorPos())
			local delay = 0
			if pos ~= last_pos or (time - last_time > 500) then
				last_time = time
				last_pos = pos
				local st = GetPreciseTicks()
				ShowTempGrid(pos)
				delay = Min(100, GetPreciseTicks() - st)
			end
			WaitWakeup(50 + delay)
		end
	end)
end

local temp_scale = const.Scale.C
local HSVtoRGB = UIL.HSVtoRGB
local RGB = RGB
local GetTemperature = sim.GetTemperature
local Clamp = Clamp
local InterpolateRGB = InterpolateRGB

local function VoxelGridCallbackRelative(x, y, zt, zs, temp0, amp)
	local z, is_terrain
	if zs > 0 then
		z = zs
	else
		z = zt
		is_terrain = true
	end
	local temp = GetTemperature(x, y, z)
	local tmp = Clamp(-temp + temp0 + amp, 0, 2*amp)
	local hue = 255 * (2*tmp) / (6*amp)
	local color = RGB(HSVtoRGB(hue, 255, 255))
	return color, z, is_terrain
end

local function VoxelGridCallbackAbsolute(x, y, zt, zs, min_temp, low_temp, avg_temp, high_temp, max_temp)
	local z, is_terrain
	if zs > 0 then
		z = zs
	else
		z = zt
		is_terrain = true
	end
	local color
	local temp = GetTemperature(x, y, z)
	if temp <= min_temp then
		color = white
	elseif temp < low_temp then
		color = InterpolateRGB(white, blue, temp - min_temp, low_temp - min_temp)
	elseif temp < avg_temp then
		color = InterpolateRGB(blue, green, temp - low_temp, avg_temp - low_temp)
	elseif temp < high_temp then
		color = InterpolateRGB(green, red, temp - avg_temp, high_temp - avg_temp)
	elseif temp < max_temp then
		color = InterpolateRGB(red, black, temp - high_temp, max_temp - high_temp)
	else
		color = black
	end
	return color, z, is_terrain
end

function ShowTempGrid(grid_pos)
	if not grid_pos or grid_pos == InvalidPos() then
		DoneObject(TempGridMesh)
		DoneObject(TempGridText)
		TempGridMesh = false
		TempGridText = false
		return
	end

	local options = CurrentModOptions or empty_table
	local radius = options.AreaRadius or 15 -- m
	local falloff = 1 -- m
	local absolute = options.TempMode ~= "Relative"
	local bbox
	local restrict_box
	local cx, cy, cz = grid_pos:xyz()
	cz = cz or GetVoxelHeight(cx, cy, true)

	local grid = TempGridMesh
	if not IsValid(grid) then
		grid = Mesh:new()
		grid:SetShader(ProceduralMeshShaders.build_grid)
		grid:SetMeshFlags(const.mfWorldSpace)
		TempGridMesh = grid
	end	
	
	local vpstr = pstr("", 64*1024, const.pstrfSaveEmpty | const.pstrfBinary)
	
	local temp = GetTemperature(cx, cy, cz)
	if absolute then
		local min_temp = (options.TempMin or -10)*temp_scale
		local low_temp = (options.TempLow or 0)*temp_scale
		local avg_temp = (options.TempAvg or 20)*temp_scale
		local high_temp = (options.TempHigh or 40)*temp_scale
		local max_temp = (options.TempMax or 50)*temp_scale
		low_temp = Max(low_temp, min_temp)
		avg_temp = Max(avg_temp, low_temp)
		high_temp = Max(high_temp, avg_temp)
		max_temp = Max(max_temp, high_temp)
		ConstructionGridFill(cx, cy, cz, bbox, restrict_box, radius, falloff, vpstr, VoxelGridCallbackAbsolute, min_temp, low_temp, avg_temp, high_temp, max_temp)
	else
		local amp = (options.TempAmp or 15)*temp_scale
		ConstructionGridFill(cx, cy, cz, bbox, restrict_box, radius, falloff, vpstr, VoxelGridCallbackRelative, temp, amp)
	end

	grid:SetMesh(vpstr)
	grid:SetUniformsList{
		-guim/2, -- DepthFadeoutDistance
		500,     -- GlowOpacity
		5*1000,  -- GlowFalloff
		3*1000,  -- GlowFeather
		1,       -- LineThickness
		5*1000,  -- LineOpacity
	}
	grid:SetPos(cx, cy, cz)
	
	local degrees = GetScaledTemperature(temp) * 1.0 / temp_scale
	local text = TempGridText
	if not IsValid(text) then
		text = PlaceObject("Text")
		text:SetTextStyle("EditorTextBold", 1500)
		text:SetDepthTest(true)
		TempGridText = text
	end
	text:SetText(string.format("%.1f", degrees))
	text:SetColor(white)
	text:SetPos(cx, cy, cz + guim)
	text:SetVisible(true)
	
	return grid, text
end

function MakeButton()
	local ids = { idOutdoor = true, idIndoor = true, idUnderRoof = true, } 
	UIForEachControl(XTemplates.HUDTimeControls, function(control, parent)
		if ids[control.Id] then
			parent.__class = "XButton"
			parent.OnPress = function(self)
				ToggleTempGrid()
			end
			parent.FXPress = "ButtonClickResearch"
			parent.Background = 0
			parent.FocusedBackground = 0
			parent.RolloverBackground = 0
			parent.PressedBackground = 0
			return true
		end
	end)
end

function OnMsg.ModsReloaded()
	MakeButton()
end