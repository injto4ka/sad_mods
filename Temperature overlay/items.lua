return {
PlaceObj('ModItemCode', {
	'CodeFileName', "Code/Script.lua",
}),
PlaceObj('ModItemOptionNumber', {
	'name', "AreaRadius",
	'DisplayName', "Radius (m)",
	'Help', "Radius of the area displayed. Larger values may be slow to display.",
	'DefaultValue', 24,
	'MinValue', 4,
	'MaxValue', 64,
}),
PlaceObj('ModItemOptionChoice', {
	'name', "TempMode",
	'DisplayName', "Display Mode",
	'Help', "Toggle between absolute or relative temperature display",
	'DefaultValue', "Absolute",
	'ChoiceList', {
		"Absolute",
		"Relative",
	},
}),
PlaceObj('ModItemOptionNumber', {
	'name', "TempAmp",
	'DisplayName', "Rel Amplitude (deg C)",
	'Help', "Defines the maximum displayed temperature difference (in Celsius, in Relative mode)",
	'DefaultValue', 16,
	'MinValue', 4,
	'MaxValue', 64,
}),
PlaceObj('ModItemOptionNumber', {
	'name', "TempMin",
	'DisplayName', "Abs Min - White  (deg C)",
	'Help', "Defines the minimum temperature displayed in white (in Celsius, in Absolute mode)",
	'DefaultValue', -10,
	'MinValue', -100,
}),
PlaceObj('ModItemOptionNumber', {
	'name', "TempLow",
	'DisplayName', "Abs Low - Blue (deg C)",
	'Help', "Defines the low temperature displayed in blue (in Celsius, in Absolute mode)",
	'MinValue', -100,
}),
PlaceObj('ModItemOptionNumber', {
	'name', "TempAvg",
	'DisplayName', "Abs Avg - Green (deg C)",
	'Help', "Defines the average temperature displayed in green (in Celsius, in Absolute mode)",
	'DefaultValue', 20,
	'MinValue', -100,
}),
PlaceObj('ModItemOptionNumber', {
	'name', "TempHigh",
	'DisplayName', "Abs High - Red (deg C)",
	'Help', "Defines the high temperature displayed in red (in Celsius, in Absolute mode)",
	'DefaultValue', 40,
	'MinValue', -100,
}),
PlaceObj('ModItemOptionNumber', {
	'name', "TempMax",
	'DisplayName', "Abs Max - Black (deg C)",
	'Help', "Defines the maximum temperature displayed in black (in Celsius, in Absolute mode)",
	'DefaultValue', 50,
	'MinValue', -100,
}),
PlaceObj('ModItemXTemplate', {
	group = "Shortcuts",
	id = "TempOverlay_Shortcuts",
	PlaceObj('XTemplateAction', {
		'ActionId', "idTempOverlayToggle",
		'ActionMode', "Game",
		'ActionName', T(393315053936, --[[ModItemXTemplate TempOverlay_Shortcuts ActionName]] "Temperature Overlay"),
		'ActionShortcut', "Ctrl-T",
		'ActionBindable', true,
		'ActionMouseBindable', false,
		'BindingsMenuCategory', "Dialogs",
		'OnAction', function (self, host, source, ...)
			ToggleTempGrid()
		end,
		'IgnoreRepeated', true,
	}),
}),
}
