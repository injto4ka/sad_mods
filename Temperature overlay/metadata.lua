return PlaceObj('ModDef', {
	'title', "Temperature overlay",
	'description', "Displays a visualization of the temperature around the mouse pointer.\n\nThere are two possible display modes, that can be selected in the mod options:\n[list]\n[*] [b]Absolute[/b] - The area colors are defined based on absolte temperature thresholds.\n[*] [b]Relative[/b] - The area colors are selected according to the temperature difference with the mouse cursor position.\n[/list]\n\nAs the available place on the hud is limited to add new buttons, the visualization is triggered by clicking on an existing element - the text specifying if the mouse cursor is indoors or outdoors, nеxt to the temperature display in the HUD.\nAlternatively, the overlay can be toggled from the keyboard by the shortcut Ctrl-T. The shortcut can be changed in the game's key bindings option menu.\n\nThe mod has options to change the displayed area size and the color temperature deviation, allowing fine control over the overlay.",
	'image', "Mod/temp_overlay/Images/temp_overlay.jpg",
	'last_changes', "The toggle shortcut can be rebound in the game's key bindings option menu.",
	'dependencies', {
		PlaceObj('ModDependency', {
			'id', "sad_commonlib",
			'title', "SAD_CommonLib",
			'version_minor', 10,
		}),
	},
	'id', "temp_overlay",
	'author', "svetlio",
	'version', 58,
	'lua_revision', 233360,
	'saved_with_revision', 352677,
	'code', {
		"Code/Script.lua",
	},
	'has_options', true,
	'has_data', true,
	'saved', 1724142771,
	'code_hash', -1829743482538428567,
	'optional_mod', true,
	'steam_id', "3312345743",
	'TagInterface', true,
})