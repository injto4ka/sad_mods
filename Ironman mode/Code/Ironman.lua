function OnMsg.GatherGameMetadata(metadata)
	if IsGameRuleActive("Ironman") then
		metadata.ironman_mode = true
		metadata.game_id = Game.id
	end
end

function OnMsg.CanSaveGameQuery(query)
	if not SavingGame and IsGameRuleActive("Ironman") then
		query.ironman = true
	end
end

function CanSaveIronmanGame()
	return Game and IsGameRuleActive("Ironman") and GetMap() ~= ""
end

function SaveIronmanGame()
	if not Game or not IsRealTimeThread() then
		return "failed"
	end
	local displayname = "Ironman " .. Game.id
	local savename = "Ironman_" .. Game.id .. ".savegame.sav"
	LoadingScreenOpen("idAutosaveScreen", "Ironman")
	local err, name, metadata = DoSaveGame(displayname, { savename = savename })
	LoadingScreenClose("idAutosaveScreen", "Ironman")
	return err, name, metadata
end

local origSaveAutosaveGame = SaveAutosaveGame
function SaveAutosaveGame(...)
	if CanSaveIronmanGame() then
		return SaveIronmanGame()
	end
	return origSaveAutosaveGame(...)
end

local origResetGameSession = ResetGameSession
function ResetGameSession(...)
	if CanSaveIronmanGame() then
		SaveIronmanGame()
	end
	origResetGameSession(...)
end

local orig_quit = quit
function quit(...)
	if CanSaveIronmanGame() and SaveIronmanGame() == nil then
		Sleep(50)
	end
	return orig_quit(...)
end

local origIsLoadButtonDisabled = IsLoadButtonDisabled
function IsLoadButtonDisabled(...)
	if CanSaveIronmanGame() then
		return true
	end
	return origIsLoadButtonDisabled(...)
end


local origQuickSaveGame = QuickSaveGame
function QuickSaveGame(...)
	if IsGameRuleActive("Ironman") then
		return "Ironman"
	end
	return origQuickSaveGame(...)
end

local origQuickLoadGame = QuickLoadGame
function QuickLoadGame(...)
	if IsGameRuleActive("Ironman") then
		return "Ironman"
	end
	return origQuickLoadGame(...)
end

local origWaitQuestion = WaitQuestion
function WaitQuestion(parent, caption, text, ...)
	if CanSaveIronmanGame() and text == T(1141, "Exit to the main menu? Any unsaved progress will be lost.") then
		return "ok"
	end
	return origWaitQuestion(parent, caption, text, ...)
end
	