return PlaceObj('ModDef', {
	'title', "Ironman mode",
	'description',
[[Introduces a new game rule "Ironman" where the game cannot be manually saved. There is only one game save that is updated on auto-save and on game exit. This means that save scumming is no more possible and if the player loses the game, he can't go back to an earlier savegame. The idea behind it is to give some additional challenge to the game.

The "Ironman" game rule is considered as a negative effect and therefore is not counted towards achievements requiring no game rules to be active.

The mod introduces the notion of positive and negative game rules. Game rules with negative effects are not counted towards achievements requiring no game rules to be active. The existing game rule "Lost cause" is thus fixed to be ignored in the achievement checks for no game rules.]],
	'image', "UI/ironman.jpg",
	'last_changes', "Beta release.",
	'dependencies', {
		PlaceObj('ModDependency', {
			'id', "sad_commonlib",
			'title', "SAD_CommonLib",
			'version_minor', 1,
		}),
	},
	'id', "sad_ironman_mode",
	'author', "svetlio",
	'version', 25,
	'lua_revision', 233360,
	'saved_with_revision', 347716,
	'code', {
		"Code/Ironman.lua",
		"Code/GameRules.lua",
	},
	'has_data', true,
	'saved', 1707263238,
	'code_hash', -8591720919443701493,
	'steam_id', "3155567175",
	'TagInterface', true,
})