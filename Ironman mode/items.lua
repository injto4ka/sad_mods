return {
PlaceObj('ModItemCode', {
	'name', "Ironman",
	'CodeFileName', "Code/Ironman.lua",
}),
PlaceObj('ModItemCode', {
	'name', "GameRules",
	'CodeFileName', "Code/GameRules.lua",
}),
PlaceObj('ModItemGameRuleDef', {
	SortKey = 100,
	description = T(147600236694, --[[ModItemGameRuleDef Ironman description]] "The game cannot be manually saved."),
	display_name = T(384879315559, --[[ModItemGameRuleDef Ironman display_name]] "Ironman"),
	effect_type = "negative",
	id = "Ironman",
}),
PlaceObj('ModItemChangeProp', {
	'TargetClass', "GameRuleDef",
	'TargetId', "SurvivorsSkillsMin",
	'TargetProp', "effect_type",
	'TargetValue', "negative",
}),
}
