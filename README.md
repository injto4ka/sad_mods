# Stranded: Alien Dawn

https://steamcommunity.com/profiles/76561198056848283/myworkshopfiles/?appid=1324130

## Teleportation

The mod introduces new devices allowing teleportation between portals.

https://steamcommunity.com/sharedfiles/filedetails/?id=3024794065


## Terrain Recovery

The terrain is slowly recovering after being altered.

https://steamcommunity.com/sharedfiles/filedetails/?id=3004997517


## Ironman mode

Introduces a new game rule "Ironman" where the game cannot be manually saved.

https://steamcommunity.com/sharedfiles/filedetails/?id=3155567175


## Temperature overlay

https://steamcommunity.com/sharedfiles/filedetails/?id=3312345743


## Lights Overlay

https://steamcommunity.com/sharedfiles/filedetails/?id=2975981035





