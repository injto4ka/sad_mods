return {
PlaceObj('ModItemCode', {
	'name', "UnitGuard",
	'CodeFileName', "Code/UnitGuard.lua",
}),
PlaceObj('ModItemXTemplate', {
	RequireActionSortKeys = true,
	group = "Infopanel Actions",
	id = "IPActionsGuard",
	PlaceObj('XTemplateGroup', {
		'comment', "Guard",
		'__context_of_kind', "UnitGuard",
	}, {
		PlaceObj('XTemplateAction', {
			'RolloverTemplate', "Rollover",
			'RolloverText', T(261438574276, --[[ModItemXTemplate IPActionsGuard RolloverText]] "Choose Target Area."),
			'RolloverTitle', T(634098009662, --[[ModItemXTemplate IPActionsGuard RolloverTitle]] "Guardian"),
			'ActionId', "idGuardMode",
			'ActionSortKey', "200",
			'ActionName', T(634098009662, --[[ModItemXTemplate IPActionsGuard ActionName]] "Guardian"),
			'ActionIcon', "Mod/SAD_GuardMode/Images/shield",
			'ActionToolbar', "ip_actions",
			'ActionState', function (self, host)
				local obj = ResolvePropObj(host.context)
				if not obj then
					return "hidden"
				end
				return obj:UIGuardActionUpdate(self)
			end,
			'OnAction', function (self, host, source, ...)
				local obj = ResolvePropObj(host.context)
				if obj then
					obj:UIGuardActionPerform(self)
				end
			end,
		}),
		}),
}),
PlaceObj('ModItemCRMaterial', {
	MaterialClass = "CRM_VisionLinePreset",
	anim_speed = 800,
	comment = "Depth test doesn't seem to work",
	depth_test = false,
	dirty = false,
	end_fade_distance = 1500000,
	fill_color = 4286958969,
	fill_width = 45,
	glow_density = 60,
	glow_segment = 10000000,
	id = "GuardVisuals",
	iterations = 10000,
	shader_id = "laser_line",
}),
PlaceObj('ModItemOptionToggle', {
	'name', "DisableManualTrigger",
	'DisplayName', "Disable RMB Trigger",
	'Help', "Don't activate the guardian mode using the right mouse button",
}),
PlaceObj('ModItemOptionToggle', {
	'name', "DisableVisuals",
	'DisplayName', "Disable Visual FX",
	'Help', "Don't activate the guardian FX",
	'OnApply', function (self, value, prev_value)
		Msg("UpdateGuardVisuals")
	end,
}),
}
