AutoResolveMethods.TryInterruptActivityCommand = true
Unit.TryInterruptActivityCommand = empty_func

local guard_text = T(634098009662, "Guardian")
local follow_text = T(301165631624, "Follow")
local origSetDraftMode = UISetDraftMode
local guim = guim

----

DefineClass.GuardedUnit = {
	__parents = { "MapObject" },
	flags = { efUnit = true },
	CanMove = return_true,
}

function GuardedUnit:IsManuallyInteractable(unit)
	local options = CurrentModOptions or empty_table
	if options.DisableManualTrigger then
		return
	end
	if not unit or not unit.can_guard or unit.CombatGroup ~= self.CombatGroup or not unit:CanMove() or not unit:IsManuallyControlled() then
		return
	end
	if not unit.can_attack and not self:CanMove() then
		return
	end
	if not IsAsyncCode() then
		unit.guard_interaction = unit.interaction_order
	end
	return unit.can_attack and guard_text or follow_text
end

function GuardedUnit:ManuallyInteract(unit)
	if unit and unit.guard_interaction and unit.guard_interaction == unit.interaction_order then
		local text = self:IsManuallyInteractable(unit)
		if rawequal(text, guard_text) or rawequal(text, follow_text) then
			CreateGameTimeThread(unit.rfnSetGuardTarget, unit, self)
		end
	end
end

----

DefineClass.UnitGuard = {
	__parents = { "Object" },
	flags = { efUnit = true },
	can_guard = true,
	guard_target = false,
	guard_interaction = false,
	last_follow_time = false,
	guard_move = false,
	guard_visuals_obj = false,
	guard_visuals_thread = false,
}

function UnitGuard:Done()
	CancelDeleteOnLoadGame(self.guard_visuals_obj)
	DoneObject(self.guard_visuals_obj)
	DeleteThread(self.guard_visuals_thread)
	self.guard_visuals_obj = nil
	self.guard_visuals_thread = nil
end

function UnitGuard:SetGuardTarget(target)
	local prev_target = self.guard_target
	if target then
		self.guard_target = target
		self.attack_move = true
		self.GetAttackStartingPoint = self.GetGuardStartingPoint
		self.GetCombatEngagementRange = self.GetGuardEngagementRange
		self.GetHUDName = self.GetGuardHUDName
		self:rfnToggleGarrisoned(false)
	elseif prev_target then
		self.guard_target = nil
		self.attack_move = nil
		self.GetAttackStartingPoint = nil
		self.GetCombatEngagementRange = nil
		self.GetHUDName = nil
	end
	self.last_follow_time = nil
	if prev_target ~= (target or false) then
		self:TryInterruptActivityCommand()
		self:ShowGuardVisuals()
		ObjModified(self)
	end
end

function UnitGuard:OnManualControlSet(set)
	if not set then
		self:SetGuardTarget(false)
	end
end

function UnitGuard:OnSetGarrisoned(garrisoned)
	if garrisoned then
		self:SetGuardTarget(false)
	end
end

function UnitGuard:OnInteractOrder()
	self:SetGuardTarget(false)
end

function UnitGuard:GetGuardEngagementRange()
	return self:GetDetectionRange()
end

function UnitGuard:GetGuardHUDName()
	local show_name = self:ShouldShowHUDName()
	return T{"<image Mod/SAD_GuardMode/Images/st_shield 1800><stunned><name>",
		stunned = self:IsStunned() and "<image UI/Hud/st_breakdown_catatonia 1800>" or "",
		name = show_name and T(345914830299, " <em><FirstName></em>") or ""
	}
end

function UnitGuard:GetGuardStartingPoint()
	return self.guard_target
end

function UnitGuard:TryGuard()
	local target = self.guard_target
	assert(target)
	if not target then
		return
	end
	local is_unit = IsValid(target)
	if not is_unit then
		target = terrain.FindPassable(target, self:GetPfClass()) or target
		if self:IsCloser(target, 1) then
			return
		end
	end
	
	local min_range = 0
	local max_range = self:GetDetectionRange() / 3
	if is_unit then
		if self:IsCloser(target, max_range) and self:CanSee(target) then
			return
		end
		min_range = guim + self:GetCollisionRadius() + target:GetCollisionRadius()
	end
	local imp = self:GetAttackImportance()
	return self:TrySetCommandImportance(imp, "CmdGuardMove", target, max_range, min_range)
end

function UnitGuard:OnObjUpdate()
	local target = self.guard_target
	if not target then return end
	if not IsValidPos(target) then
		self:SetGuardTarget(false)
		return
	end
	local elapsed = self.last_follow_time  and (GameTime() - self.last_follow_time)
	if elapsed and (elapsed == 0 or elapsed < 3000 + self:Random(6000, "Guard")) then
		return
	end
	return self:TryGuard()
end

function UnitGuard:GetTargetAttraction(target)
	local aggression_time = table.get(self.guard_target, "attacked_history", target)
	if aggression_time then
		return 100 + Max(0, 200 - (GameTime() - aggression_time)/100)
	end
	return 100
end

local function GuardAttackFilter(self, attack_target, guard_target, disengage_dist)
	if not attack_target:IsCloser(guard_target, self:GetAttackRange() + disengage_dist) then return end
	if self:IsAttackTargetIgnored(attack_target) then return end
	if not self:IsAttackTarget(attack_target) then return end
	if not attack_target:CanBeAttacked(self) then return end
	return true
end
	
function UnitGuard:FindGuardAttackTarget(guard_target, min_delay)
	if not self.can_attack then
		return
	end
	local now = GameTime()
	if min_delay and (now - self.attack_move_last_search < min_delay) then
		return
	end
	local disengage_dist = self:GetCombatEngagementRange()
	if not self:IsCloser(guard_target, disengage_dist) then
		return
	end
	self.attack_move_last_search = now
	local attack_target = self:FindAttackTarget(nil, guard_target)
	if not attack_target and IsValid(guard_target) then
		attack_target = guard_target:GetValidAttacker() or guard_target:GetValidAttackTarget()
		if attack_target and not GuardAttackFilter(self, attack_target, guard_target, disengage_dist) then
			return
		end
	end
	return attack_target
end

function UnitGuard:CmdGuardMove(guard_target, max_range, min_range)
	-- TODO: all this should be set when starting the guarding mode
	self:ExitHolder(guard_target)
	self.last_follow_time = GameTime()
	local is_unit = IsValid(guard_target)
	local attack_target
	self:ClearIgnoredAttackTargets()
	local rand_max_range = is_unit and max_range > min_range and (min_range + self:Random(max_range - min_range, "Guard")) or max_range
	local orig_OnGotoStep = self.OnGotoStep
	self.OnGotoStep = function(self, ...)
		if not self.attack_target then
			attack_target = self:FindGuardAttackTarget(guard_target, 50)
			if attack_target then
				return true
			end
			if is_unit and self:IsCloser(guard_target, rand_max_range) and self:CanSee(guard_target) then
				return true
			end
		end
		return orig_OnGotoStep(self, ...)
	end
	self.guard_move = true
	local dtors = self:PushDestructor(function(self)
		self.OnGotoStep = nil
		self.Goto = nil
		self.attack_move_last_search = nil
		self.guard_move = nil
	end)

	local GameTime = GameTime
	while true do
		self:Goto(guard_target)
		if not IsValid(attack_target) then
			break
		end
		local count = 0
		while attack_target do
			local st = GameTime()
			self:WaitAttackTarget(attack_target)
			if st == GameTime() then
				count = count + 1
				if count == 3 then
					self:PlayIdleAnim(500)
					break
				end
			else
				count = 0
			end
			attack_target = self:FindGuardAttackTarget(guard_target)
		end
	end
	
	self:PopAndCallDestructor(dtors)
end

function UnitGuard:rfnSetGuardTarget(target)
	if target == self then
		target = terrain.FindPassable(target)
	end
	self:SetGuardTarget(target)
	self:SetActivity(false)
	if target and self:TryGuard() then
		PlayFX("MoveCommand", "start", self, nil, ResolveVisualPos(target))
	end
end

function UnitGuard:UIGuardActionUpdate(action)
	if not IsValidPos(self) or GameState.Tutorial or not self:IsManuallyControlled() then
		return "hidden"
	end
	local icon, text
	if self.guard_target then
		icon = "Mod/SAD_GuardMode/Images/shield_cancel"
		text =  T(967444875712, "Cancel")
	elseif PathVisualsThread then
		icon = "Mod/SAD_GuardMode/Images/shield_confirm"
		text =  T(6895, "Confirm")
	else
		icon = "Mod/SAD_GuardMode/Images/shield"
		text =  T(261438574276, "Choose Target Area.")
	end
	action.ActionIcon = icon
	action.RolloverText = Untranslated("<center>") .. text
	if not self.can_attack then
		return "disabled"
	end
end

function UnitGuard:UIGuardActionPerform(action)
	if self.guard_target then
		NetSyncEvent("ObjFunc", self, "rfnSetGuardTarget", false)
	elseif PathVisualsThread then
		ExitTargetPosMode()
		NetSyncEvent("ObjFunc", self, "rfnSetGuardTarget", self)
	else
		EnterGuardUIMode{self}
	end
end

local function ExtractUnits(bin)
	local units = {}
	for _, unit in ipairs(bin) do
		if IsValidPos(unit) and unit.can_attack and unit:IsManuallyControlled() then
			units[#units + 1] = unit
		end
	end
	return units
end

function UnitGuard.CreateBinActions(bin, host, skip_bin_actions_for_work_actions)
	if not bin or GameState.Tutorial then return end
	local units = ExtractUnits(bin)
	if #units == 0 then return end
	local template = table.get(XTemplates, "IPActionsGuard", 1, 1)
	if not template then return end
	local action = {}
	table.append(action, template)
	action.ActionState = function(self)
		return units[1]:UIGuardActionUpdate(self)
	end
	action.OnAction = function(self)
		local unit = units[1]
		if unit.guard_target then
			NetSyncEvent("MultiObjFunc", units, "rfnSetGuardTarget", false)
		elseif PathVisualsThread then
			ExitTargetPosMode()
			for _, unit in ipairs(units) do
				NetSyncEvent("ObjFunc", unit, "rfnSetGuardTarget", unit)
			end
		else
			EnterGuardUIMode(units)
		end
	end
	XAction:new(action, host)
	return true
end

local dir_packed = RGBA(254, 127, 127, 0)
local function GuardAppendLineSegmentPointwise(_pstr, dist)--pt0, pt1, dir_vector, prev_distance, prev_dir_packed)
	local half_dist = dist / 2
	local inside, outside = 0, 2
	local AppendVertex = _pstr.AppendVertex

	AppendVertex(_pstr, 0, 0, 0,         dir_packed, outside, 0)
	AppendVertex(_pstr, 0, 0, 0,         dir_packed,  inside, 0)
	AppendVertex(_pstr, half_dist, 0, 0, dir_packed,       1, half_dist)

	AppendVertex(_pstr, 0, 0, 0,         dir_packed, outside, 0)
	AppendVertex(_pstr, dist, 0, 0,      dir_packed, outside, dist)
	AppendVertex(_pstr, half_dist, 0, 0, dir_packed,       1, half_dist)

	AppendVertex(_pstr, dist, 0, 0,      dir_packed,  inside, dist)
	AppendVertex(_pstr, 0, 0, 0,         dir_packed,  inside, 0)
	AppendVertex(_pstr, half_dist, 0, 0, dir_packed,       1, half_dist)

	AppendVertex(_pstr, half_dist, 0, 0, dir_packed,       1, half_dist)
	AppendVertex(_pstr, dist, 0, 0,      dir_packed, outside, dist)
	AppendVertex(_pstr, dist, 0, 0,      dir_packed,  inside, dist)
end

local function ShowGuardVisualsProc(self)
	local options = CurrentModOptions or empty_table
	local prev_hash
	local last_dist = max_int
	local mesh_str_cache
	local k = 1
	local zoffset = guim/4
	while true do
		local obj = self.guard_visuals_obj
		local target = self.guard_target
		if not IsValid(target) then
			DoneObject(obj)
			CancelDeleteOnLoadGame(obj)
			self.guard_visuals_obj = nil
			self.guard_visuals_thread = nil
			return
		end
		if options.DisableVisuals
		or not self.selected and not target.selected
		or not IsValidPos(self)
		or not IsValidPos(target)
		or self:IsCloser(target, guim)
		or not self:IsCloser(target, 50*guim) then
			if IsValid(obj) then
				obj:SetVisible(false)
			end
			prev_hash = false
		else
			if not IsValid(obj) then
				local mat = CR_CreateMaterial("GuardVisuals")
				if not mat then return end
				obj = PlaceObject("Mesh")
				obj:ClearMeshFlags(const.mfWorldSpace)
				obj:SetShader(ProceduralMeshShaders.soft_mesh)
				obj:SetCRMaterial(mat:Clone())
				DeleteOnLoadGame(obj)
				self.guard_visuals_obj = obj
				prev_hash = false
			end
			if not mesh_str_cache then
				mesh_str_cache = {}
				for i=1,2 do
					mesh_str_cache[i] = pstr("", 1024, const.pstrfSaveEmpty | const.pstrfBinary)
				end
			end
			local hash = GetPosHash(self, GetPosHash(target))
			if prev_hash ~= hash then
				prev_hash = hash
				local delta = self:TimeToPosInterpolationEnd()
				local vis_pos = self:GetVisualPos(delta)
				local delta1 = target:TimeToPosInterpolationEnd()
				local vis_target = target:GetVisualPos(delta1)
				local delta_max = Max(delta, delta1)
				local dist = vis_pos:Dist(vis_target)
				if dist < guim then
					obj:SetVisible(false)
				else
					if abs(last_dist - dist) > guim then
						last_dist = dist
						local mesh_str = mesh_str_cache[k]
						mesh_str:clear()
						k = (k == 1) and 2 or 1
						GuardAppendLineSegmentPointwise(mesh_str, dist)
						local mat = obj:GetCRMaterial()
						mat.length = dist
						mat:Recreate()
						obj:SetUniformsPstr(mat:GetDataPstr())
						obj:SetMesh(mesh_str)
					end
					local pitch, yaw = GetPitchYaw(vis_pos, vis_target)
					if not obj:IsValidPos() or not obj:GetVisible() then
						delta = 0
						delta_max = 0
					end
					vis_pos:InplaceAddZ(zoffset)
					obj:SetPos(vis_pos, delta)
					obj:SetRollPitchYaw(0, pitch, yaw, delta_max)
					obj:SetVisible(true)
				end
			end
		end
		WaitMsg("UpdateGuardVisuals", 100)
	end
end

function UnitGuard:ShowGuardVisuals()
	Msg("UpdateGuardVisuals")
	if IsValidThread(self.guard_visuals_thread) then
		return
	end
	self.guard_visuals_thread = CreateGameTimeThread(ShowGuardVisualsProc, self)
end

function OnMsg.SelectionAdded(obj)
	obj.selected = true
	Msg("UpdateGuardVisuals")
end

function OnMsg.SelectionRemoved(obj)
	obj.selected = false
	Msg("UpdateGuardVisuals")
end

function OnMsg.LoadGame()
	Msg("UpdateGuardVisuals")
end

----

AppendClass.Human = {
	__parents = { "GuardedUnit", "UnitGuard" },
}

AppendClass.UnitAnimal = {
	__parents = { "GuardedUnit", "UnitGuard" }
}

AppendClass.Robot = {
	__parents = { "GuardedUnit", "UnitGuard" }
}

local Human_GetMainStatusIcon = Human.GetMainStatusIcon
function Human:GetMainStatusIcon()
	local icon1 = Human_GetMainStatusIcon(self)
	if icon1 == "UI/Hud/st_draft" and self.guard_target then
		icon1 = "Mod/SAD_GuardMode/Images/st_shield"
	end
	return icon1
end

local origHuman_GetIPActivityCurrent = Human.GetIPActivityCurrent
function Human:GetIPActivityCurrent()
	local text = origHuman_GetIPActivityCurrent(self)
	if self.guard_target then
		local loc_id = TGetID(text) or 0
		if loc_id == 919577672561 or loc_id == 895404216948 then
			return T(634098009662, "Guardian")
		end
	end
	return text
end

----

MapVar("PathVisualsThread", false)
MapVar("PathVisualsLines", false)
PersistableGlobals.PathVisualsThread = false

local function ResolveGuardTarget(unit, mouse_pos, mouse_obj)
	if not IsValidPos(unit) then return end
	return (mouse_obj == unit or IsKindOf(mouse_obj, "GuardedUnit") and mouse_obj.CombatGroup == unit.CombatGroup) and mouse_obj or terrain.FindPassable(mouse_pos, unit, -1, unit)
end

function ClearGuardSelection()
	local thread =  PathVisualsThread
	if PathVisualsLines then
		SetHighlightTarget()
		UISetDraftMode = origSetDraftMode
		DoneObjects(PathVisualsLines)
		PathVisualsLines = false
		PathVisualsThread = false
		DeleteThread(thread)
	end
end

local function UISafeDraftMode(state)
	if not state then
		ExitTargetPosMode()
		return origSetDraftMode()
	end
end

local guard_mode_UI

function EnterGuardUIMode(units)
	if #units == 0 then return end

	ClearGuardSelection()
	UISetDraftMode = UISafeDraftMode
	
	PathVisualsLines = {}
	PathVisualsThread = CreateMapRealTimeThread(function(units)
		local cursor_pos_list, unit_pos_list = {}, {}
		while true do
			local mouse_pos = GetCursorPos()
			local mouse_obj = GetPreciseCursorObj()
			for i, unit in ipairs(units) do
				local target = ResolveGuardTarget(unit, mouse_pos, mouse_obj)
				SetHighlightTarget(IsValid(target) and target)
				local new_cursor_pos = IsPoint(target) and terrain.FindPassable(target, unit, -1, unit) or ResolvePos(target)
				local new_unit_pos = GetPosHash(unit)
				local old_cursor_pos, old_unit_pos = cursor_pos_list[i], unit_pos_list[i]
				if new_cursor_pos and (new_cursor_pos ~= old_cursor_pos or new_unit_pos ~= old_unit_pos) then
					cursor_pos_list[i], unit_pos_list[i] = new_cursor_pos, new_unit_pos
					local path, reachable
					if target ~= unit then
						path, reachable = CalcVisualPath(unit, new_cursor_pos)
					end
					if not next(path) then
						DoneObject(PathVisualsLines[i])
					else
						PathVisualsLines[i] = InplaceUnitPath(path, reachable and green or red, PathVisualsLines[i])
					end
				end
			end
			Sleep(50)
		end
	end, units)

	guard_mode_UI = true
	ExitTargetPosMode()
	SetInGameInterfaceMode("IModePickTargetPos", {
		callback = function()
			local mouse_pos = GetCursorPos()
			local mouse_obj = GetPreciseCursorObj()
			for i_, unit in ipairs(units) do
				local target = ResolveGuardTarget(unit, mouse_pos, mouse_obj)
				if target then
					NetSyncEvent("ObjFunc", unit, "rfnSetGuardTarget", target)
				end
			end
		end,
	})
	for _, unit in ipairs(units) do
		ObjModified(unit)
	end
end

function OnMsg.LoadGame()
	ClearGuardSelection()
end

function OnMsg.IGIModeChanging(prev_mode, mode)
	if mode ~= "IModePickTargetPos" and guard_mode_UI then
		ClearGuardSelection()
		UIReactivateDraftMode()
		guard_mode_UI = false
	end
end