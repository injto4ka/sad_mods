DefineClass.LightOverlay = {
	__parents = { "Overlay" },
	overlay_icon = "UI/Hud/re_button",
	overlay_name = T(945177924111, "Light"),
	overlay_description = T(945177924111, "Light"),
	
	OpenOnGameStart = false,
	sort_key = 9,
	
	BuildMenuIcon = "UI/Icons/Build Menu/lights",
	BuildMenuPos = 10,
}

MapVar("LightOverlayInfo", false)

function HideLightInfo(bld)
	local infos = LightOverlayInfo
	local info = infos and infos[bld]
	if not info then return end
	infos[bld] = nil
	DoneObject(info.los)
end

function ShowLightInfo(bld, show)
	local infos = LightOverlayInfo
	if show == nil then
		show = IsOverlayActive("LightOverlay")
	end
	local range = show and bld.LightingComponent and not bld.LightMultiple and bld.LightRadius or 0
	local hash = GetPosHash(bld)
	local info = infos and infos[bld]
	local prev_hash = info and info.hash
	local prev_range = info and info.range or 0
	if prev_range == range and prev_hash == hash then
		return
	end
	local ok, v_pstr
	if range > 0 then
		ok, v_pstr = pcall(bld.WaitGetLosArea, bld)
	end
	if not ok or not v_pstr then
		if info then
			infos[bld] = nil
			DoneObject(info.los)
		end
		return
	end

	infos = infos or {}
	LightOverlayInfo = infos
	info = info or {}
	infos[bld] = info
	info.range = range
	info.hash = hash

	local los_obj = info.los
	if not IsValid(los_obj) then
		los_obj = TempMesh:new()
		los_obj:SetShader(ProceduralMeshShaders.soft_mesh)
		los_obj:SetDepthTest(false)
		los_obj:SetUniforms(const.Combat.RangeDepthFadeoutDistance or -guim)
		los_obj:SetMeshFlags(const.mfWorldSpace)
		info.los = los_obj
	end
	los_obj:SetMesh(v_pstr)
	los_obj:SetVisible(true)
	bld:Attach(los_obj)
end

function ShowOverlay.LightOverlay()
	for _, bld in ipairs(UIPlayer.labels.LightingComponents) do
		if not IsKindOf(bld, "ComplexSlabWallObject") then
			ShowLightInfo(bld, true)
		end
	end
end

function HideOverlay.LightOverlay()
	for bld, info in pairs(LightOverlayInfo) do
		DoneObject(info.los)
	end
	LightOverlayInfo = false
end

function OnMsg.BuildingPlaced(bld)
	ShowLightInfo(bld)
end
OnMsg.DeconstructionDone = HideLightInfo
OnMsg.LoadGame = HideOverlay.LightOverlay

----

function LightingComponent:ForEachLabel(callback, ...)
	callback("LightingComponents", ...)
end

local LightingComponentOnMoved = LightingComponent.OnMoved or empty_func
function LightingComponent:OnMoved(...)
	ShowLightInfo(self)
	return LightingComponentOnMoved(self, ...)
end

function SavegameFixups.LightingComponentLabel()
	MapForEach("map", "LightingComponent", function(bld)
		if bld.LightingComponent and bld.player then
			bld.player:AddToLabel("LightingComponents", bld)
		end
	end)
end

AppendClass.LightingObject = {
	properties = {
		{ category = "Lighting", id = "LightRadiusMin", name = "Light Radius Min",  editor = "number", default = const.PassTileSize / 2 },
		{ category = "Lighting", id = "LightShowArea",  name = "Light Show Area",   editor = "bool", default = true },
	},
}

local origIsLosAreaEnabled = LightingObject.IsLosAreaEnabled
function LightingObject:IsLosAreaEnabled()
	if not IsOverlayActive("LightOverlay") then
		return
	end
	return origIsLosAreaEnabled(self)
end

function LightingObject:GetLosAreaParams()
	local parabola, pitch_max, pitch_min
	local spot_name = self.LightSpot
	local custom_dir = self.LightDirection
	local max_range = self.LightRadius
	local min_range = const.PassTileSize / 2
	local fov = self.LightFov
	return max_range, min_range, fov, parabola, pitch_max, pitch_min, spot_name, custom_dir
end

local IsKindOf = IsKindOf
local origShowStripedLOSVisualization = ShowStripedLOSVisualization
local origRangeLosAreaShow = RangeLosAreaShow
function RangeLosAreaShow(obj, ...)
	if IsKindOf(obj, "LightingObject") then
		ShowStripedLOSVisualization = empty_func
	else
		ShowStripedLOSVisualization = origShowStripedLOSVisualization
	end
	return origRangeLosAreaShow(obj, ...)
end
