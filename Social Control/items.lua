return {
PlaceObj('ModItemHappinessFactor', {
	Description = T(636786299400, --[[ModItemHappinessFactor Roommates Description]] "Has an increased affection toward people sleeping in the same room."),
	DisplayName = T(438564759161, --[[ModItemHappinessFactor Roommates DisplayName]] "Roommates"),
	Expiration = true,
	Happiness = 5,
	OnAdd = function (self, owner, ...)
		OnRoommateTrigger(owner)
	end,
	OnStackLimitReached = function (self, owner, ...)
		return false
	end,
	StackLimit = 1,
	comment = "Triggers on sleep start/end towards prople sleeping in the same room.",
	group = "Social",
	id = "Roommates",
	msg_reactions = {
		PlaceObj('MsgReaction', {
			Event = "UnitChangeSleep",
			Handler = function (self, unit, sleeping, sleep_time)
				TryTriggerRoommate(unit)
			end,
			param_bindings = false,
		}),
	},
}),
PlaceObj('ModItemChangeProp', {
	'comment', "Avoid having other love interests if already having one",
	'TargetClass', "StoryBit",
	'TargetId', "LoveInterest",
	'TargetProp', "Prerequisites",
	'EditType', "Append To Table",
	'TargetValue', {
		PlaceObj('CheckExpression', {
			Expression = function (self, obj) return LoveInteresetCheck(obj) end,
			param_bindings = false,
		}),
	},
}),
PlaceObj('ModItemConstDef', {
	comment = "Social bonus on effect trigger",
	group = "Social",
	id = "RoommateRelationReward",
	value = 5,
}),
PlaceObj('ModItemOptionToggle', {
	'name', "Roommates",
	'DisplayName', "Roommates",
	'Help', "People sleeping in the same room receive affection bonus to each other.",
	'DefaultValue', true,
}),
PlaceObj('ModItemOptionToggle', {
	'name', "FriendsWithBenefits",
	'DisplayName', "Friends with Benefits",
	'Help', "Roomates have a higher chance of becoming lovers. Has no effect if the Roommates option isn't enabled.",
	'DefaultValue', true,
}),
PlaceObj('ModItemOptionToggle', {
	'name', "TilDeathDoUsPart",
	'DisplayName', "Til Death Do Us Part",
	'Help', "Married couples would not fall in love with somebody else.",
}),
PlaceObj('ModItemOptionToggle', {
	'name', "TraditionalSociety",
	'DisplayName', "Traditional Society",
	'Help', "Love interest between people of the same sex will stay hidden.",
}),
PlaceObj('ModItemCode', {
	'name', "SocialControl",
	'CodeFileName', "Code/SocialControl.lua",
}),
PlaceObj('ModItemLocTable', {
	'language', "German",
	'filename', "Mod/SAD_SocialControl/Localization/German.csv",
}),
}
