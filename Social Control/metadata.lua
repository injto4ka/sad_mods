return PlaceObj('ModDef', {
	'title', "Social Control",
	'description', "[h1]Additional social control[/h1]\n\nThe player has various mod options to influence the relationships of the survivors.\n\n[h2]Mod options[/h2]\n\n[list]\n[*][b]Roommates[/b]\nPeople sleeping in the same room receive affection bonus to each other\n[*][b]Friends with Benefits[/b]\nRoommates have an increased probability to become lovers.\n[*][b]Til Death Do Us Part[/b]\nMarried couples would not fall in love with somebody else.\n[/list]\n\n[h2]Mod details[/h2]\n\n[list]\n[*] Increased the distance at which the survivors can engage in social interactions.\n[*] Can be safely added or removed without breaking saved games.\n[*] Includes a German translation.\n[/list]\n\nThe mod is made entirely by community request.\n\nCover art made by [b]Distracted Desk Dweller[/b]\nhttps://www.youtube.com/@dddweller",
	'image', "Mod/SAD_SocialControl/Images/love.jpg",
	'last_changes', "Official release.",
	'SpellCheck', true,
	'dependencies', {
		PlaceObj('ModDependency', {
			'id', "sad_commonlib",
			'title', "SAD_CommonLib",
			'version_major', 1,
			'version_minor', 9,
		}),
	},
	'id', "SAD_SocialControl",
	'author', "svetlio",
	'version_major', 1,
	'version', 30,
	'lua_revision', 233360,
	'saved_with_revision', 352677,
	'code', {
		"Code/SocialControl.lua",
	},
	'loctables', {
		{
			filename = "Mod/SAD_SocialControl/Localization/German.csv",
			language = "German",
		},
	},
	'has_options', true,
	'has_data', true,
	'saved', 1739224493,
	'code_hash', 1855580040007322060,
	'optional_mod', true,
	'steam_id', "3415576734",
	'TagGameplay', true,
})