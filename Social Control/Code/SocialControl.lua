local options = CurrentModOptions or empty_table
local InteractionRand = InteractionRand

function OnRoommateTrigger(unit)
	if not unit.room or not options.Roommates then
		return
	end
	local consts = const.Social or empty_table
	local has_love_chance = options.FriendsWithBenefits
	local relation_reward = consts.RoommateRelationReward or 5
	local try_love
	for _, other in ipairs(unit.player.labels.Survivors) do
		if other ~= unit and other.room == unit.room and other:IsSleeping() then
			local opinion = unit:ChangeOpinion(other, relation_reward) or 0
			if has_love_chance and opinion > 0 and not unit:HaveRelationship(other) and InteractionRand(100, "FriendsWithBenefits") < opinion then
				try_love = table.create_add(try_love, other)
			end
		end
	end
	if try_love then
		if #try_love > 1 then
			table.shuffle(try_love, InteractionRand(nil, "FriendsWithBenefits"))
			try_love = table.sortby_dist(try_love, unit)
		end
		local other_social = unit.other_social or nil
		for _, other in ipairs(try_love) do
			unit.other_social = other
			if TryActivateStoryBit("LoveInterest", unit, true) then
				other_social = other
				break
			end
		end
		unit.other_social = other_social
	end
end

function TryTriggerRoommate(unit)
	if not options.Roommates or not unit.room or not unit:IsKindOf("Human") then
		return
	end
	for _, other in ipairs(unit.player.labels.Survivors) do
		if other ~= unit and other.room == unit.room and other:IsSleeping() then
			unit:AddHappinessFactor("Roommates")
			return
		end
	end
end

function LoveInteresetCheck(unit)
	if options.TilDeathDoUsPart and unit:GetOtherByRelationship("Spouse", "Lover/Fiance/Spouse") then
		return
	end
	if options.TraditionalSociety and unit.other_social and unit.Gender == unit.other_social.Gender then
		return
	end
	return true
end

FixChangedModID("SAD_LoveConquersAll", CurrentModId)