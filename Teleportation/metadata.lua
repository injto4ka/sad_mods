return PlaceObj('ModDef', {
	'title', "Teleportation",
	'description', "The mod introduces new devices allowing teleportation between portals. The basic device is a Teleportation pad, that can be built everywhere and is used as a connection to other such pads. But in order to function, a Teleportation hub is needed, which is a large unique structure, that is more expensive and difficult to build. During its operation the hub is consuming an amount of power proportional to the number of supported pads.\nIn order to be unlocked, the Teleportation technology has to be first researched, which requires a considerable amount of time and intellect.\n\nThe mod doesn't import any new art. All the used assets are from different parts of the vanilla game.\n\nThe mod is localized in all officially supported languages. But the translation is generated and it's not verified by a human. I can only tell that the French is close to the English text. I will be happy to receive and apply corrections from native speakers.\nNote: the game needs to be restarted after language changes in order to see the correct mod texts.\n\nThe mod has a \"soft\" dependency to [url=https://steamcommunity.com/sharedfiles/filedetails/?id=3115193939]SAD_CommonLib[/url]. It can be safely ran without it, but some fixes and new features wont be available.",
	'image', "Mod/degwEV/Images/TeleportHub.jpg",
	'last_changes', 'Added a new game rule "Free Teleportation" to remove the additional power consumption added from the active pads.\nReplaced the high-pitch working sound with another more suitable one.',
	'id', "degwEV",
	'author', "svetlio",
	'version_major', 1,
	'version_minor', 3,
	'version', 159,
	'lua_revision', 233360,
	'saved_with_revision', 352677,
	'code', {
		"Building/TeleportationHub.lua",
		"Building/TeleportationPad.lua",
		"Code/Fixups.lua",
		"Code/Teleportation.lua",
	},
	'loctables', {
		{
			filename = "Localization/French.csv",
			language = "French",
		},
		{
			filename = "Localization/Brazilian.csv",
			language = "Brazilian",
		},
		{
			filename = "Localization/German.csv",
			language = "German",
		},
		{
			filename = "Localization/Japanese.csv",
			language = "Japanese",
		},
		{
			filename = "Localization/Korean.csv",
			language = "Koreana",
		},
		{
			filename = "Localization/Polish.csv",
			language = "Polish",
		},
		{
			filename = "Localization/Russian.csv",
			language = "Russian",
		},
		{
			filename = "Localization/Schinese.csv",
			language = "Schinese",
		},
		{
			filename = "Localization/Spanish.csv",
			language = "Spanish",
		},
	},
	'has_data', true,
	'saved', 1736096612,
	'code_hash', -8679283106787038743,
	'steam_id', "3024794065",
	'TagBuildings', true,
	'TagGameplay', true,
	'TagTranslations', true,
})