local InvalidZ = const.InvalidZ
local AsyncRand = AsyncRand
local platform_plane = PlaneFromPoints(0,0,-10, 0,1,-10, 1,0,-10, true)

NextTeleportSound = 0
local TeleportSoundCooldown = 1000

----

DefineClass.TeleportationHubBuilding = {
	__parents = { "CompositeBuilding" },
	flags = { efApplyToGrids = false, efCollision = false, efSelectable = true, efAttackable = false, gofDamageable = false },
	
	construction_controller = "CompositeConstructionController",
	
	SelectionParticlesKind = "big_corners",
	
	composite_part_target = "TeleportationHub",
	composite_part_groups = { "Teleportation" },
	composite_part_names = {
		"Base", "Platform", "Decal", "Pivot",
		"Link1", "Ring1", "Ball1",
		"Link2", "Ring2", "Ball2",
		"Link3", "Ring3", "Ball3"},
	composite_part_main = "Base",
	
	labels = {
		"TeleportationHub",
		"Teleporters",
	},
	
	links = false,
	balls = false,
	pivot = false,
	platform = false,
	lightings = false,
	
	fx_thread = false,
	teleportation_ready = false,
	activation_thread = false,
	activation_fx = false,
	rotation_speed = 0,
	update_power_consumption = false,
	pads_display = false,
	
	require_surfaces = false, -- the surfaces are contained in the attached columns
	CanBeTargeted = empty_func, -- the child elements are the actual targets
	StrandGetArea = empty_func,
	GetApproachBox = empty_func,
}

function TeleportationHubBuilding:InitBodyParts()
	self:UpdateActivationFX(false)
	self.platform = self:GetPart("Platform")
	self.pivot = self:GetPart("Pivot")
	local links, balls = {}, {}
	for i=1,3 do
		links[i] =  self:GetPart("Link" .. i)
		balls[i] =  self:GetPart("Ball" .. i) 
		balls[i]:SetClipPlane(PlaneFromPoints(0, 0, 0, 0, 0, 1, 0, 1, 0, true))
	end
	self.links = links
	self.balls = balls
	self:SetColorModifier(RGBRM(0xff006666, -50, 50), true)
	self.platform:SetClipPlane(platform_plane)
	self:UpdateActivationFX(self.teleportation_ready)
	return CompositeBuilding.InitBodyParts(self)
end

function TeleportationHubBuilding:OnBuildingPlaced()
	local collision = PlaceObject("UnitCollisionBox1", nil, const.cofComponentAttach)
	collision:SetAttachOffset(0, 0, 1500)
	collision:SetGameFlags(const.gofAllowLOS)
	collision:SetScale(250)
	self:Attach(collision)
	self.collision = collision
	return CompositeBuilding.OnBuildingPlaced(self)
end

function TeleportationHubBuilding:CanActivate()
	if not self.teleportation_ready then
		return
	end
	return CompositeBuilding.CanActivate(self)
end

function TeleportationHubBuilding:Done()
	self:UpdateActivationFX(false)
	self:ShowHubs(false)
	DoneObjects(self.lightings)
	DoneObject(self.collision)
	DeleteThread(self.fx_thread)
	DeleteThread(self.activation_thread)
	self.links = nil
	self.balls = nil
	self.platform = nil
	self.pivot = nil
	self.lightings = nil
	self.collision = nil
	self.teleportation_ready = nil
	self.fx_thread = nil
	self.activation_thread = nil
end

function TeleportationHubBuilding:FlightInitObstacle()
	return FlightInitBox(self, self:GetPassBox())
end

function TeleportationHubBuilding:TeleportStartRandomFX()
	local lightings = self.lightings
	if not lightings then
		lightings = {
			PlaceObject("FX_Lightning_MW_01"),
			PlaceObject("FX_Lightning_MW_02"),
			PlaceObject("FX_Lightning_MW_03"),
			PlaceObject("FX_Lightning_MW_04"),
		}
		self.lightings = lightings
	end
	local lighting, idx = table.rand(lightings)
	local min_scale, max_scale = 45, 50
	local pivot = self.pivot
	local x, y, z = pivot:GetVisualPosXYZ()
	local idx = AsyncRand(3)
	local a = pivot:GetVisualAngle() + (idx - 2) * 120 * 60 + 20 * 60 + AsyncRand(-5*60, 5*60)
	local s = AsyncRand(min_scale, max_scale)
	local dt = AsyncRand(400, 600)
	lighting:SetScale(s)
	lighting:SetPos(x, y, z + 100)
	lighting:SetAngle(a)
	lighting:SetAngle(a + dt * self.rotation_speed / 1000, dt)
	lighting:InitTextureAnimation()
	lighting:SetVisible(true)
	self.balls[1 + idx]:SetSound("electricity_wires_spark", 2000, 0, 3*guim)--"electricity_wires_work", 3000, 300, 5*guim)
	--PlayFX("TeleportationHubRandom_Pivot", "start", self.pivot)
	return dt
end

function TeleportationHubBuilding:TeleportEndRandomFX()
	for _, lighting in ipairs(self.lightings) do
		lighting:SetVisible(false)
	end
	--for _, ball in ipairs(self.balls) do ball:StopSound(300) end
	return AsyncRand(2000)
end

function TeleportationHubBuilding:UpdateActivationFX(active)
	active = active or false
	if self.activation_fx == active then return end
	self.activation_fx = active
	
	for _, ball in ipairs(self.balls) do
		PlayFX("TeleportationHubActive_Ball", active and "start" or "end", ball)
	end
	if active then
		CreateGameTimeThread(function(self)
			Sleep(500)
			if self.teleportation_ready then
				self:SetSound("windturbinecarbon_rotationloopRPM1", 2000, 1000, 2*guim)
				PlaySound("militarypod_flarethrow", 1000, 0, false, self:GetVisualPos())
			end
		end, self)
	else
		self:StopSound(1000)
	end
	PlayFX("TeleportationHubActive_Base", active and "start" or "end", self)
	PlayFX("TeleportationHubActive_Pivot", active and "start" or "end", self.pivot)
	if active and not IsValid(self.fx_thread) then
		self.fx_thread = CreateGameTimeThread(function(self)
			Sleep(3333)
			while self.teleportation_ready do
				local speed = Max(1000, GetTimeFactor())
				local duration = self:TeleportStartRandomFX()
				Sleep(duration * speed / 1000)
				local dt = self:TeleportEndRandomFX()
				Sleep(dt * speed / 1000)
			end
		end, self)
	elseif not active then
		self:TeleportEndRandomFX()
		DeleteThread(self.fx_thread)
	end
end

function TeleportationHubBuilding:GetTeleportationPads()
	return table.get(self.player, "labels", "TeleportationPad")
end

function TeleportationHubBuilding:TeleportActivationStep()
	if self.activation_thread ~= CurrentThread() then return end
	local working = self.working or false
	local rotation_speed = self.rotation_speed
	local dt = 200
	local force_accel = 15*60
	local activation_accel = force_accel / 2
	local friction = 20
	local accel = (working and force_accel or 0) - rotation_speed * friction / 100
	local teleportation_ready = working and accel <= activation_accel
	if self.teleportation_ready ~= teleportation_ready then
		self.teleportation_ready = teleportation_ready
		self:UpdateActivationFX(teleportation_ready)
		for _, pad in ipairs(self:GetTeleportationPads()) do
			pad:UpdateWorking()
		end
		if SelectedObj == self then
			CreateRealTimeThread(self.ShowHubs, self, true)
		end
	end
	local speed_delta = accel * dt / 1000 - 10
	if speed_delta ~= 0 then
		rotation_speed = rotation_speed + speed_delta
		if rotation_speed <= 0 then
			self.rotation_speed = nil
			return
		end
		rotation_speed = Min(rotation_speed, 179*60 * 1000 / dt)
		self.rotation_speed = rotation_speed
	end
	local pivot = self.pivot
	if not IsValid(pivot) then return end
	local angle = pivot:GetVisualAngleLocal()
	angle = angle + rotation_speed * dt / 1000
	pivot:SetAngle(angle, dt)
	return dt
end

function TeleportationHubBuilding:OnSetWorking(working)
	if working then
		if not IsValid(self.activation_thread) then
			self.activation_thread = CreateGameTimeThread(function(self)
				while true do
					local dt = self:TeleportActivationStep()
					if not dt then return end
					Sleep(dt)
				end
			end, self)
		end
	end
end

function TeleportationHubBuilding:UpdatePowerConsumption()
	self.update_power_consumption = false
	if IsGameRuleActive("FreeTeleport") then
		return
	end
	local current_consumption = self.base_PowerConsumption
	local new_consumption = self:GetClassValue("base_PowerConsumption")
	local count = 0
	for _, pad in ipairs(self:GetTeleportationPads()) do
		if pad.on and not pad:IsVirtual() and not IsBeingDestructed(pad) then
			new_consumption = round(new_consumption + new_consumption / 10, const.ResourceScale)
			count = count + 1
		end
	end
	if new_consumption == current_consumption then
		return
	end
	self.base_PowerConsumption = new_consumption
	self.PowerConsumption = new_consumption
	self:ChangedModifier("PowerConsumption")
	if self.IsPowerConsumer then
		self:UpdatePowerElement()
	end
	ObjModified(self)
end

function TeleportationHubBuilding:OnObjUpdate()
	if self.update_power_consumption then
		self:UpdatePowerConsumption()
	end
end

local materials
local function GetTeleportMaterial(mat_id)
	materials = materials or {
		active = CRM_VisionLinePreset:new{
			anim_speed = 800,
			end_fade_distance = 1500000,
			fill_color = 0xff00ffff,
			fill_width = 45,
			glow_density = 60,
			glow_segment = 10000000,
			iterations = 10000,
		},
		inactive = CRM_VisionLinePreset:new{
			anim_speed = 0,
			end_fade_distance = 1500000,
			fill_color = 0xffaaaaaa,
			fill_width = 25,
			glow_density = 0,
			glow_segment = 0,
			iterations = 10000,
		},
	}
	return materials[mat_id]
end

function TeleportationHubBuilding:ShowHubs(show)
	local pads_display = self.pads_display or {}
	self.pads_display = pads_display

	for pad, info in pairs(pads_display) do
		if not IsValidPos(pad) then
			DoneObject(obj)
			CancelDeleteOnLoadGame(obj)
			pads_display[pad] = nil
		else
			info.visible = false
		end
	end

	local pads = show and self:GetTeleportationPads()
	if next(pads) then
		local offsetz = guim/2
		local pos0 = self:GetVisualPos():AddZ(offsetz)
		for _, pad in ipairs(pads) do
			if IsValidPos(pad) then
				local info = pads_display[pad]
				if not info then
					info = {}
					pads_display[pad] = info
				end
				local mesh = info.mesh
				if not IsValid(mesh) then
					mesh = PlaceObject("Mesh")
					mesh:SetMeshFlags(const.mfWorldSpace)
					mesh:SetDepthTest(false)
					mesh:SetVisible(false)
					DeleteOnLoadGame(mesh)
				end
				local dist = info.dist
				local pos = pad:GetVisualPos():AddZ(offsetz)
				local hash = xxhash(pos0, pos)
				if hash ~= info.hash or mesh ~= info.mesh then
					local mesh_str = pstr("", 1024, const.pstrfSaveEmpty | const.pstrfBinary)
					dist = CRTrail_AppendLineSegmentPointwise(mesh_str, pos0, pos)
					mesh:SetMesh(mesh_str)
					mesh:SetPos(pos0)
				end 
				local mat = info.mat
				local mat_id = pad.working and "active" or "inactive"
				if mat_id ~= info.mat_id or mesh ~= info.mesh then
					mat = GetTeleportMaterial(mat_id):Clone()
					mat.length = dist
					mesh:SetCRMaterial(mat)
				elseif info.dist ~= dist then
					mat.length = dist
					mat:Recreate()
					mesh:SetUniformsPstr(mat:GetDataPstr())
				end
				info.mesh = mesh
				info.mat_id = mat_id
				info.mat = mat
				info.dist = dist
				info.hash = hash
				info.visible = true
			end
		end
	end
	for _, info in pairs(pads_display) do
		if IsValid(info.mesh) then
			info.mesh:SetVisible(info.visible)
		end
	end
end

function OnMsg.SelectedObjChange(obj, prev)
	if IsKindOf(obj, "TeleportationHubBuilding") then
		obj:ShowHubs(true)
	end
	if IsKindOf(prev, "TeleportationHubBuilding") then
		prev:ShowHubs(false)
	end
end

function GetWorkingTeleportationHub(player)
	for _, hub in ipairs(table.get(player, "labels", "TeleportationHub")) do
		if hub.teleportation_ready then
			return hub
		end
	end
end

----

DefineClass.TeleportationPortal = {
	__parents = { "PFTunnel", },
	tunnel_type = PF_TUNNEL_DOOR,
	tunnel_cost = 1000,
	player = false,
	UpdatePowerConsumption = empty_func,
}

function TeleportationPortal:TraverseTunnel(obj, ...)
	return obj:TraversePortal(self, ...)
end

function TeleportationPortal:GetTeleportationHubs()
	return table.get(self.player, "labels", "TeleportationHub") or empty_table
end

function TeleportationPortal:IsTeleportationActive()
	for _, hub in ipairs(self:GetTeleportationHubs()) do
		if hub.teleportation_ready then
			return true
		end
	end
end

function TeleportationPortal:CanWork()
	return self:IsTeleportationActive()
end

function TeleportationPortal:GatherIPStatuses(statuses)
	if not self:IsTeleportationActive() then
		statuses[InfopanelStatuses.TeleportationHubNotActive] = true
	end
end

function TeleportationPortal:OnSetWorking(working)
	self:UpdateTunnel()
	self:UpdatePowerConsumption()
end

function TeleportationPortal:AddPFTunnel(update_hash)
	if not self.working then
		return
	end
	local x1, y1, z1 = self:GetPosXYZ()
	z1 = z1 or InvalidZ
	local tunnel_cost, tunnel_type, tunnel_flags = self.tunnel_cost, self.tunnel_type, self.tunnel_flags
	for _, hub in ipairs(self:GetTeleportationHubs()) do
		if hub.teleportation_ready then
			local x2, y2, z2 = hub:GetPosXYZ()
			z2 = z2 or InvalidZ
			pf.AddTunnel(self, x1, y1, z1, x2, y2, z2, tunnel_cost, tunnel_type, tunnel_flags)
			pf.AddTunnel(self, x2, y2, z2, x1, y1, z1, tunnel_cost, tunnel_type, tunnel_flags)
			if update_hash then
				self:NetUpdateHash("AddPFTunnel", hub)
			end
		end
	end
end

----

DefineClass.TeleportationPadBuilding = {
	__parents = { "CompositeBuilding", "TeleportationPortal" },
	flags = { efApplyToGrids = false, efCollision = false, efSelectable = true, efAttackable = false, gofDamageable = false },
	
	labels = {
		"TeleportationPad",
		"Teleporters",
	},
	
	composite_part_target = "TeleportationPad",
	composite_part_groups = { "Teleportation" },
	composite_part_names = { "Base", "Platform" },
	composite_part_main = "Base",

	ConstructHelper = "HumanMale",
	
	orient_to_terrain = true,
}

function TeleportationPadBuilding:Done()
	self:UpdatePowerConsumption()
end

function TeleportationPadBuilding:UpdatePowerConsumption()
	for _, hub in ipairs(self:GetTeleportationHubs()) do
		hub.update_power_consumption = true
	end
end

function TeleportationPadBuilding:InitBodyParts()
	self.platform = self:GetPart("Platform")
	self.platform:SetClipPlane(platform_plane)
	self:SetColorModifier(RGBRM(0xff006666, -50, 50), true)
	return CompositeBuilding.InitBodyParts(self)
end

----

Unit.OnPortalTraversed = empty_func
AutoResolveMethods.OnPortalTraversed = true

function Unit:TraversePortal(portal, end_point, param)
	self:ExecuteUninterruptable(function(self, portal, end_point)
		PlayFX("Teleportation", "start", self, portal)
		self.moving = nil
		self:StopFX("Moving")
		local dt = 500
		local scale = self:GetScale()
		local opacity = self:GetOpacity()
		local speed = self:GetAnimSpeed(1)
		local x1, y1, z1 = self:GetVisualPosXYZ()
		self:SetScale(1, dt)
		self:SetOpacity(1, dt)
		self:SetAnimSpeed(1, 1, dt)
		self:SetPos(x1, y1, z1 + guim/2, dt)
		Sleep(dt)
		PlayFX("Teleportation", "hit", self, portal)
		local x2, y2, z2 = end_point:xyz()
		z2 = z2 or terrain.GetHeight(x2, y2)
		self:SetPos(x2, y2, z2 + guim/2)
		self:SetScale(scale, dt)
		self:SetOpacity(opacity, dt)
		self:SetAnimSpeed(1, speed, dt)
		self:SetPos(x2, y2, z2, dt)
		PlayFX("Teleportation", "hit", self, portal)
		local time = RealTime()
		if NextTeleportSound < time then
			NextTeleportSound = time + TeleportSoundCooldown
			PlaySound("wp_turret-laser_shot-single", 300, 0, false, point(x1, y1, z1), 5)
			PlaySound("wp_turret-laser_shot-single", 300, 0, false, point(x2, y2, z2), 5)
		end
		Sleep(dt)
		self:SetPos(end_point)
		PlayFX("Teleportation", "end", self, portal)
		self:OnPortalTraversed(portal, end_point, param)
	end, portal, end_point)
	return true
end

-- fix for tamed animals being led
function Unit:FollowLeaderStep(leader, min_dist, max_dist)
	if not IsValid(leader) then return end
	local res, status = self:Goto(leader, max_dist, min_dist, true)
	if not IsValid(leader) then return end
	if res and pf.GetLinearDist(self, leader) > max_dist then
		self:Goto(leader)
	end
	self:PlayIdleAnim(500)
	return IsValid(leader)
end

-- fix for flying units ignoring the teleporter
function FlyingMovable:IsShortPath(walk_excess, max_walk_dist, min_flight_dist)
	if self:IsPathPartial() then
		return
	end
	local last = self:GetPathPointCount() > 0 and self:GetPathPoint(1)
	if not last then
		return true
	end
	local short_path_len
	if GetWorkingTeleportationHub(self.player) then
		local dist = pf.GetLinearDist(self, last)
		local flight_speed = self.FlightSpeedMax
		local move_speed = self:GetSpeed()
		short_path_len = MulDivRound(dist, move_speed, flight_speed)
	else
		local dist = pf.GetLinearDist(self, last)
		if max_walk_dist and dist > max_walk_dist then
			return
		end
		short_path_len = Min(max_walk_dist or max_int, dist * (100 + (walk_excess or 0)) / 100)
	end
	local ignore_tunnels = true
	local path_len = self:GetPathLen("3D", 1, Max(min_flight_dist or 0, short_path_len), ignore_tunnels)
	return path_len <= short_path_len
end

