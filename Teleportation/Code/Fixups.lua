function SavegameFixups.RemoveLeakedTeleportFX()
	for preset, actor_to_target in pairs(FX_Assigned) do
		if preset.id == "TeleportationPad_Working" then
			for actor, target_to_fx in pairs(actor_to_target) do
				for target, fx in pairs(target_to_fx) do
					preset:DestroyFX(actor, target)
				end
			end
		end
	end
	MapForEach("map", "TeleportationPad", function(pad)
		pad:DestroyAttaches("ParSystemBase")
		if pad.on then
			pad:TurnOff()
			CreateGameTimeThread(function(pad)
				Sleep(50)
				pad:TurnOn()
			end, pad)
		end
	end)
end

function SavegameFixups.RemoveOldTeleportInfo()
	MapForEach("map", "TeleportationHubBuilding", function(hub)
		for _, mesh in ipairs(hub.pads_display) do
			DoneObject(mesh)
			CancelDeleteOnLoadGame(mesh)
		end
		hub.pads_display = nil
	end)
end