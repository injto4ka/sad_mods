return {
PlaceObj('ModItemCode', {
	'name', "TerrainRecovery",
	'CodeFileName', "Code/TerrainRecovery.lua",
}),
PlaceObj('ModItemOptionToggle', {
	'name', "DisableRecoverPlants",
	'DisplayName', "Disable Recover Plants",
}),
PlaceObj('ModItemOptionToggle', {
	'name', "DisableRecoverGrass",
	'DisplayName', "Disable Recover Grass",
}),
PlaceObj('ModItemOptionToggle', {
	'name', "DisableRecoverHeight",
	'DisplayName', "Disable Recover Height",
}),
PlaceObj('ModItemOptionToggle', {
	'name', "DisableRecoverSoil",
	'DisplayName', "Disable Recover Soil",
}),
PlaceObj('ModItemStoryBit', {
	Category = "",
	Enabled = true,
	Image = "Mod/gQuyVvs/UI/TerrainRecovery.jpg",
	NotificationRolloverText = T(484389628034, --[[ModItemStoryBit TerrainRecovery_WhatsNew NotificationRolloverText]] "Insect nests are included in the restoration process."),
	NotificationRolloverTitle = T(258272714945, --[[ModItemStoryBit TerrainRecovery_WhatsNew NotificationRolloverTitle]] "What's new"),
	NotificationTitle = T(816056437275, --[[ModItemStoryBit TerrainRecovery_WhatsNew NotificationTitle]] "Terrain Recovery"),
	Text = T(859676948459, --[[ModItemStoryBit TerrainRecovery_WhatsNew Text]] "Insect nests are included in the restoration process. For those nests, that already exists on the map when the game is started, the terrain is generated based on the surroundings.\nThe generation requires a new game and will not be triggered for old saved games."),
	Title = T(586303468196, --[[ModItemStoryBit TerrainRecovery_WhatsNew Title]] "Version 1.2"),
	Trigger = "",
	comment = "Insect nests are included in the restoration process.",
	id = "TerrainRecovery_WhatsNew",
}),
}
