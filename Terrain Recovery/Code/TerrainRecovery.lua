MapVar("TerrainBuff", {})
MapVar("TerrainRestoreSeed", false)
MapVar("TerrainRestoreIdx", 0)
MapVar("TerrainRestoreSuspended", false)

local retry_delay = 12 * const.HourDuration
local retry_delay_rand = 12 * const.HourDuration
local reapply_delay = 2 * const.HourDuration
local voxel_radius2 = const.SlabSizeX * const.SlabSizeX / 4
local voxel_height = const.SlabSizeZ
local max_scavengeable_radius = 25 * guim
local max_nest_radius = 50 * guim
local replant_delay = 6 * const.DayDuration
local replant_delay_rand = 4 * const.DayDuration
local pass_angle = const.MaxPassableTerrainSlope

local LockEnumRadius = const.LockEnumRadius
local efConstructionLock = const.efConstructionLock
local efVisible = const.efVisible
local efAffectsConstruction = const.efAffectsConstruction
local gofPermanent = const.gofPermanent

local GetMinMaxVoxelHeight = GetMinMaxVoxelHeight
local MapGetFirst = MapGetFirst

GetMapsPassHash = empty_func

-- restore modes: "type", "grass", "height", "plants"

local restore_inv = {
	type = function(ibox)
		terrain.RestoreType(ibox)
		return 33
	end,
	grass = function(ibox)
		terrain.RestoreType(ibox)
		return 33
	end,
	height = function(ibox, rebuild)
		terrain.RestoreHeight(ibox)
		MapForEach(ibox, CObject.ClearCachedZ)
		if rebuild then
			terrain.RebuildPassability(ibox)
			--DbgAddTerrainRect(ibox) DbgAddVector(ibox:Center())
			return 79
		end
		return 19
	end,
}
local is_vertex_grid = {
	height = true,
}
local restore_step = {
	grass = 1,
	height = 2,
}
local restore_step_lim = {
	height = 6,
}
local restore_step_pct = {
	height = 1,
}
local restore_max_diff = {
	height = const.HeightTileSize / const.TerrainHeightScale,
}
local restore_cooldown = {
	type = 5000,
	grass = 1000,
}
local restore_cooldown_threshold = {
	type = 50000,
	grass = 1000,
}
local restore_tile = {
	type = const.TypeTileSize,
	grass = const.GrassTileSize,
	height = const.HeightTileSize,
	plants = 1,
}
local restore_dist = {
	type = 4,
	grass = 1,
	height = 4,
	plants = 3*guim,
}
local dbg_color = {
	type = red,
	grass = green,
	height = blue,
	plants = yellow,
}
local dbg_offset = {
	type = point(-50, -50, -50),
	grass = point(50, 50, 50),
	height = point(0, 0, 0),
}

local mode_options = {
	plants = "DisableRecoverPlants",
	grass = "DisableRecoverGrass",
	height = "DisableRecoverHeight",
	type = "DisableRecoverSoil",
}

local options = CurrentModOptions
local function IsRestoreDisabled(mode)
	local option_name = mode_options[mode]
	return option_name and options[option_name]
end

local function GetNextSeed()
	local seed = BraidRandom(TerrainRestoreSeed or MapLoadRandom)
	TerrainRestoreSeed = seed
	return seed
end

local function InitRecoveryBuffer(mode, idx)
	local buffers = TerrainBuff or {}
	TerrainBuff = buffers
	local buff = buffers[mode]
	if not buff then
		buff = {
			radius = {},
			copy = createtable(4096),
			orig = createtable(4096),
			retry = createtable(4096),
			list = createtable(0, 4096),
			count = 0,
			cooldown = 0,
			mode = mode,
			idx = idx or false,
		}
		buffers[mode] = buff
		buffers[#buffers + 1] = buff
	end
	return buff
end

local function TerrainChange(box, mode, begin)
	if TerrainRestoreSuspended then return end
	local grid = editor.GetGridRef(mode)
	if not grid or not box or box:IsEmpty2D() then return end
	--DbgAddBox(box:SetInvalidZ())
	local x0, y0, x1, y1 = box:xyxy()
	local tile = restore_tile[mode]
	x0 = x0 / tile
	y0 = y0 / tile
	x1 = (x1 - 1) / tile
	y1 = (y1 - 1) / tile
	local buff = InitRecoveryBuffer(mode)
	local copy = buff.copy
	local orig = buff.orig
	local retry = buff.retry
	local list = buff.list
	local count = buff.count
	local retry_time = GameTime() + retry_delay
	for y = y0,y1 do
		local o = (y << 16) | x0
		for x = x0,x1 do
			if not orig[o] then
				if begin then
					copy[o] = grid:get(x, y)
				else
					local prev = copy[o]
					if prev then
						copy[o] = nil
						local current = grid:get(x, y)
						if prev ~= current then
							count = count + 1
							orig[o] = prev
							retry[o] = retry_time
							list[count] = o
						end
					end
				end
			end
			o = o + 1
		end
	end
	buff.count = count
	buff.idx = false
end

----

local TR_pos, TR_box = point(), box()

local function TestLock(lock)
	if (lock.lock_flags & LOCK_PLATFORM) ~= 0 then
		return TR_box:Dist2D2(lock) < voxel_radius2
	end
	local lock_block_box = lock.lock_block_box or lock:GetLocalBlockBox()
	if not lock_block_box then
		return TR_box:PointInside(lock)
	end
	return ConstructionBoxIntersect(TR_box, lock_block_box, lock)
end

local function TestObstacle(obstacle)
	return obstacle:IsObstructing(TR_box)
end

local function TestNest(obstacle)
	local radius = obstacle:GetTerrainChangeRadius()
	return radius and IsCloser2D(obstacle, TR_pos, radius)
end

function TerrainRecoveryInvalidate(mode, ibox, rebuild)
	local restore_func = restore_inv[mode] or empty_func
	return restore_func(ibox, rebuild)
end

local function RemoveIdx(buff, idx)
	local count = buff.count
	local list = buff.list
	local offset = list[idx]
	buff.orig[offset] = nil
	buff.retry[offset] = nil
	buff.radius[offset] = nil
	list[idx] = list[count]
	list[count] = nil
	buff.count = count - 1
	--print(buff.mode, count)
end
		
function TerrainRecoveryGet()
	local time = GameTime()
	local buffers = TerrainBuff or {}
	TerrainBuff = buffers
	local tri = TerrainRestoreIdx or 0
	local buff, found
	for i=1,#buffers do
		tri = (tri == #buffers) and 1 or (tri + 1)
		buff = buffers[tri]
		if buff and buff.count > 0 and (time - (buff.cooldown or 0) > 0) and not IsRestoreDisabled(buff.mode) then
			found = true
			break
		end
	end
	TerrainRestoreIdx = tri
	if not found then
		return false, 10000
	end
	local idx = buff.idx
	if not idx then
		buff.idx = 1
		table.shuffle(buff.list, GetNextSeed())
		return
	end
	local mode, list, retry, orig, count = buff.mode, buff.list, buff.retry, buff.orig, buff.count
	local offset, orig_value
	local tests = 256
	while tests > 0 do
		if idx > count then
			idx = 1
		end
		offset = list[idx] or false
		local retry_time = retry[offset] or -1
		if time - retry_time > 0 then
			orig_value = orig[offset]
			if not orig_value then
				RemoveIdx(buff, idx)
			end
			break
		end
		idx = idx + 1
		tests = tests - 1
	end
	if not orig_value then
		buff.idx = idx
		if tests == 0 then
			buff.cooldown = time + 999
		end
		return
	end
	buff.idx = idx + 1
	return mode, buff, idx, offset, orig_value
end

function TerrainRecoveryApply(mode, buff, idx, offset, orig_value)
	local tile = restore_tile[mode] or 1
	local dist_to_locks = (restore_dist[mode] or 1) * tile + (buff.radius[offset] or 0)

	local x, y
	if tile == 1 then
		x = offset & 0xffffffff
		y = (offset >> 32) & 0xffffffff
	else
		x = offset & 0xffff
		y = (offset >> 16) & 0xffff
	end
	local mx, my = x * tile, y * tile
	if not is_vertex_grid[mode] then
		mx, my = mx + tile/2, my + tile/2
	end
	TR_pos:InplaceSet(mx, my)

	local minh, maxh = GetMinMaxVoxelHeight(TR_pos)
	TR_box:InplaceSet(mx - tile/2, my - tile/2, minh, mx + tile/2, my + tile/2, maxh)
	TR_box:InplaceGrow(dist_to_locks, dist_to_locks, voxel_height)
	
	local obstacle = MapGetFirst(TR_pos, dist_to_locks + LockEnumRadius + tile/2, "ConstructionLock", efConstructionLock | efVisible, nil, gofPermanent, TestLock)
	if not obstacle then
		obstacle = MapGetFirst(TR_pos, dist_to_locks + max_scavengeable_radius + tile/2, "ScavengeableObject", efVisible | efAffectsConstruction, nil, gofPermanent, TestObstacle)
	end
	if not obstacle and (mode == "type" or mode == "plants") then
		obstacle = MapGetFirst(TR_pos, dist_to_locks + max_nest_radius + tile/2, "TerritorialNest", efVisible, TestNest)
	end
	if obstacle then
		--DbgAddSegment(TR_pos, obstacle, red)
		--DbgAddCircle(TR_pos, dist_to_locks, dbg_color[mode]) DbgAddText(mode .. ":" .. tostring(orig_value), TR_pos)
		buff.retry[offset] = GameTime() + retry_delay + GetNextSeed() % retry_delay_rand
		return
	end
	--DbgAddVector(TR_pos)
	--DbgAddVector(TR_pos + (dbg_offset[mode] or point30), guim, dbg_color[mode]) DbgAddText(mode .. ":" .. tostring(orig_value), TR_pos)
	local new_value = orig_value
	if mode == "plants" then
		local angle = GetNextSeed() % (360*60)
		local plant = PlacePlant(orig_value, TR_pos, angle)
		plant:SetGameFlags(const.gofPermanent)
	else
		TR_box:InplaceGrow(tile/2 - dist_to_locks, tile/2 - dist_to_locks, 0) -- will be used later when invalidating the terrain
		buff.retry[offset] = GameTime() + 1 + GetNextSeed() % (reapply_delay + 1)
		local grid = editor.GetGridRef(mode)
		local step = restore_step[mode]
		if step then
			local pct = restore_step_pct[mode]
			local current = grid:get(x, y)
			if pct then
				local diff = abs(current - orig_value)
				step = step + MulDivRound(diff, pct, 100)
				local lim = restore_step_lim[mode]
				if lim then
					step = Min(step, lim)
				end
			end
			if current > orig_value then
				new_value = Max(current - step, orig_value)
			elseif current < orig_value then
				new_value = Min(current + step, orig_value)
			end
		end
		local max_diff = restore_max_diff[mode]
		if max_diff then
			for dy = -1,1 do
				for dx = -1,1 do
					if (dx ~= 0 or dy ~= 0) and (dx == 0 or dy == 0) and abs(grid:get(x+dx, y+dy) - new_value) > max_diff then
						return
					end
				end
			end
		end
		grid:set(x, y, new_value)
	end
	local restored = new_value == orig_value
	if restored then
		RemoveIdx(buff, idx)
		buff.idx = idx
	end
	
	local cooldown = restore_cooldown[mode] or 0
	if cooldown > 0 then
		local threshold = restore_cooldown_threshold[mode]
		if threshold then
			cooldown = Lerp(cooldown, 0, buff.count, threshold)
		end
		if cooldown > 0 then
			buff.cooldown = GameTime() + cooldown
		end
	end
	return TR_box, restored
end

MapGameTimeRepeat("TerrainRecovery", nil, function(dt)
	local mode, buff, idx, offset, orig_value = TerrainRecoveryGet()
	if not mode then
		return buff or 19 -- retry
	end
	Sleep(5)
	local ibox, rebuild = TerrainRecoveryApply(mode, buff, idx, offset, orig_value)
	if not ibox then
		return 19
	end
	Sleep(5)
	return TerrainRecoveryInvalidate(mode, ibox, rebuild)
end)

----

if FirstLoad then
	orig_ChangeTerrainInBox = false
	orig_ChangeTerrainInCircle = false
	orig_SetGrassDensityInBox = false
	orig_AsyncGridSetTerrain = false
end

orig_ChangeTerrainInBox = rawget(_G, "orig_ChangeTerrainInBox") or ChangeTerrainInBox
function ChangeTerrainInBox(box, ...)
	TerrainChange(box, "type", true)
	local inv = orig_ChangeTerrainInBox(box, ...)
	TerrainChange(box, "type", false)
	return inv
end

orig_ChangeTerrainInCircle = rawget(_G, "orig_ChangeTerrainInCircle") or ChangeTerrainInCircle
function ChangeTerrainInCircle(center, inner_radius, outer_radius, ...)
	local bbox = box(center, outer_radius)
	TerrainChange(bbox, "type", true)
	local inv = orig_ChangeTerrainInCircle(center, inner_radius, outer_radius, ...)
	TerrainChange(bbox, "type", false)
	return inv
end

orig_ChangeTerrainToTypesInCircle = rawget(_G, "orig_ChangeTerrainToTypesInCircle") or ChangeTerrainToTypesInCircle
function ChangeTerrainToTypesInCircle(center, grid, noiseGrid, threshold, terrain1, terrain2, innerRadius, outerRadius)
	local bbox = box(center, outerRadius or innerRadius)
	TerrainChange(bbox, "type", true)
	local inv = orig_ChangeTerrainToTypesInCircle(center, grid, noiseGrid, threshold, terrain1, terrain2, innerRadius, outerRadius)
	TerrainChange(bbox, "type", false)
	return inv
end

orig_SetGrassDensityInBox = rawget(_G, "orig_SetGrassDensityInBox") or SetGrassDensityInBox
function SetGrassDensityInBox(box, ...)
	TerrainChange(box, "grass", true)
	orig_SetGrassDensityInBox(box, ...)
	TerrainChange(box, "grass", false)
end

orig_AsyncGridSetTerrain = rawget(_G, "orig_AsyncGridSetTerrain") or AsyncGridSetTerrain
function AsyncGridSetTerrain(raster_params)
	local hbox, tbox, gbox
	if raster_params.height_grid then
		local tile = restore_tile.height
		local size = raster_params.height_grid:size()
		hbox = box(raster_params.pos, size * tile / 2 + tile)
		TerrainChange(hbox, "height", true)
	end
	if raster_params.type_grid then
		local tile = restore_tile.type
		local size = raster_params.type_grid:size()
		tbox = box(raster_params.pos, size * tile / 2 + tile)
		TerrainChange(tbox, "type", true)
	end
	if raster_params.grass_grid then
		local tile = restore_tile.grass
		local size = raster_params.grass_grid:size()
		gbox = box(raster_params.pos, size * tile / 2 + tile)
		TerrainChange(gbox, "grass", true)
	end
	
	local err, inv_bbox = orig_AsyncGridSetTerrain(raster_params)
	
	if hbox then
		TerrainChange(hbox, "height", false)
	end
	if tbox then
		TerrainChange(tbox, "type", false)
	end
	if gbox then
		TerrainChange(gbox, "grass", false)
	end
	
	return err, inv_bbox
end

function ScavengeableDebris:Done()
	MapDelete(self, 10*guim, "Decal", function(decal)
		return decal.class:starts_with("DecSpaceshipDebris")
	end)
end

function OnMsg.PlantDeleted(plant)
	if not IsValidPos(plant) or plant:GetGameFlags(const.gofPermanent) == 0 then return end
	local x, y, z = plant:GetPosXYZ()
	if z then return end -- not on the terrain
	local plant_id = plant:GetPlantType() or ""
	local plant_def = Plants[plant_id]
	if not plant_def then return end -- unknown plant
	local buff = InitRecoveryBuffer("plants", 1)
	local offset = y << 32 | x
	if buff.orig[offset] then return end -- something already at this place?
	local replant_time = GameTime() + replant_delay + GetNextSeed() % (replant_delay_rand + 1)
	buff.radius[offset] = GetEntityBBox(plant_def.MatureEntity):Radius2D()
	buff.retry[offset] = replant_time
	buff.orig[offset] = plant_id
	local count = buff.count + 1
	buff.list[count] = offset
	buff.count = count
end

function RegenTerrainTypes(center, radius, max_radius, rem_types, noise_name)
	center = ResolvePos(center)
	max_radius = max_radius or max_int
	noise_name = noise_name or "TerrainSeason"
	local rem_idx = {}
	for _, ttype in ipairs(rem_types) do
		local idx = GetTerrainTextureIndex(ttype)
		if idx then
			rem_idx[idx] = true
		end
	end
	local ttypes = {}
	local GetTerrainType, IsPointInBounds, RotateRadius, Clamp, guim = terrain.GetTerrainType, terrain.IsPointInBounds, RotateRadius, Clamp, guim
	for i=1,100 do
		table.clear(ttypes, true)
		local steps = Clamp((radius * 44 / 7) / guim, 8, 90)
		local step = 360 * 60 / steps
		local inside
		for j=1,steps do
			local x, y = RotateRadius(radius, j*step, center, true)
			if IsPointInBounds(x, y) then
				local idx = GetTerrainType(x, y)
				if rem_idx[idx] then
					inside = true
				else
					ttypes[idx] = (ttypes[idx] or 0) + 1
				end
			end
		end
		if not inside or radius >= max_radius then
			break
		end
		radius = radius + guim
	end
	local tlist = table.keys(ttypes, true)
	table.sort(tlist, function(t1, t2)
		return ttypes[t1] > ttypes[t2]
	end)
	local recover_idx1, recover_idx2 = tlist[1], tlist[2]
	if not recover_idx1 then
		return
	end
	local recover_weight1 = ttypes[recover_idx1]
	local recover_weight2 = recover_idx2 and ttypes[recover_idx2] or 0
	local threshold2 = recover_weight1 * 255 / (recover_weight2 + recover_weight1)
	local npreset = NoisePresets[noise_name]
	local noise, nw, nh
	local buff = InitRecoveryBuffer("type")
	local orig = buff.orig
	local retry = buff.retry
	local list = buff.list
	local count = buff.count
	local retry_time = GameTime() + retry_delay
	local tgrid = editor.GetGridRef("type")
	local get = tgrid.get
	local IsCloser2D = IsCloser2D
	local ttile = const.TypeTileSize
	radius = radius + 10*guim
	local xc, yc = center:xy()
	local y0, y1 = (yc-radius)/ttile, (yc+radius)/ttile
	local x0, x1 = (xc-radius)/ttile, (xc+radius)/ttile
	for y=y0,y1 do
		local my = y * ttile + ttile / 2
		local o = (y << 16) | x0
		for x=x0,x1 do
			if not orig[o] then
				local mx = x * ttile + ttile / 2
				if IsCloser2D(xc, yc, mx, my, radius) then
					local idx = get(tgrid, x, y)
					if rem_idx[idx] then
						local recover_idx = recover_idx1
						if recover_idx2 then
							if not noise and npreset then
								noise = npreset:GetNoise(xxhash(center))
								nw, nh = noise:size()
							end
							if noise then
								local nx, ny = x % nw, y % nh
								local nv = get(noise, nx, ny)
								if nv > threshold2 then
									recover_idx = recover_idx2
								end
							end
						end
						count = count + 1
						orig[o] = recover_idx
						retry[o] = retry_time
						list[count] = o
					end
				end
			end
			o = o + 1
		end
	end
	buff.count = count
	buff.idx = false
	if noise then
		noise:free()
	end
end

local function RegenExistingTerrain()
	MapForEach("map", "TerritorialNest", function(nest)
		local nest_types = { "AlienEarth_03", "AlienEarth_04", "AlienEarth_04_C1", "A_Grass_Red", nest.terrain_type1, nest.terrain_type2 }
		RegenTerrainTypes(nest, nest:GetTerrainCoverRange() + 4*guim, nest.max_range + nest.terrain_range_border + 4*guim, nest_types)
	end)
end

OnMsg.PostNewMapLoaded = RegenExistingTerrain

----

local origTerritorialNestDone = TerritorialNest.Done
function TerritorialNest:Done()
	origTerritorialNestDone(self)
	MapForEach(self, self.max_range, "DecPuddle_03", "DecPuddle_02", "DecPuddle_01", function(obj)
		CreateGameTimeThread(function()
			local dt = const.DayDuration
			obj:SetOpacity(0, dt)
			Sleep(dt)
			DoneObject(obj)
		end)
	end)
	MapForEach(self, self.max_range, "FXSource", function(obj)
		if obj.FxPreset == "SFX_close-nests" then
			DoneObject(obj)
		elseif obj.FxPreset == "VFX_AlienHaze_Red" then
			CreateGameTimeThread(function()
				obj:ForEachAttach(CObject.StopEmitters)
				Sleep(const.HourDuration)
				DoneObject(obj)
			end)
		end
	end)
end

----

local function ShowWhatsNewNotification()
	CreateRealTimeThread(function()
		WaitSurvivorsPlaced()
		local preset = StoryBits["TerrainRecovery_WhatsNew"]
		if not preset then return end
		CurrentModStorageTable = CurrentModStorageTable or {}
		if CurrentModStorageTable.whats_new_shown ~= preset.comment then
			CurrentModStorageTable.whats_new_shown = preset.comment
			ForceActivateStoryBit("TerrainRecovery_WhatsNew", nil, "immediate", nil, true)
			WriteModPersistentStorageTable()
		end
	end)
end
	
OnMsg.NewMapLoaded = ShowWhatsNewNotification
OnMsg.LoadGame = ShowWhatsNewNotification

----

function SavegameFixups.FixTerrainBuff()
	local i = 1
	for mode, buff in sorted_pairs(TerrainBuff) do
		buff.mode = mode
		TerrainBuff[i] = buff
		i = i + 1
	end
end

---- DEBUG

if Platform.debug then

if FirstLoad then
	DbgTerrainRecoveryViewThread = false
	DbgTerrainRecoveryViewMode = false
	DbgTerrainRecoveryViewObjs = false
	DbgTerrainRecoveryViewTime = 0
end

function DbgTerrainRecoveryView(mode)
	mode = mode or false
	if mode and not mode_options[mode] or GetMap() == "" then
		mode = false
	end
	DbgTerrainRecoveryViewMode = mode
	if not mode then
		DeleteThread(DbgTerrainRecoveryViewThread)
		local orig_print = print
		print = empty_func
		DbgShowConstructionLocks()
		for obj, mesh in pairs(DbgMeshObjs) do
			DoneObject(mesh)
		end
		DbgMeshObjs = false
		for obj in pairs(DbgTerrainRecoveryViewObjs) do
			if obj.dbg_territorial_range then
				obj:AsyncCheatRange()
			end
		end
		DbgTerrainRecoveryViewObjs = false
		DbgClear(true)
		print = orig_print
		return
	end
	if IsValidThread(DbgTerrainRecoveryViewThread) then
		Wakeup(DbgTerrainRecoveryViewThread)
		return
	end
	DbgTerrainRecoveryViewThread = CreateMapRealTimeThread(function()
		local last_pos = point()
		local meshes = DbgMeshObjs or {}
		DbgMeshObjs = meshes
		local rposi = point()
		DbgSetVectorZTest(false)

		while true do
			local mode = DbgTerrainRecoveryViewMode
			local buff = InitRecoveryBuffer(mode)
			local tile = restore_tile[mode] or 1
			local new_objs = false
			local mx, my, mz = GetCursorPos():xyz()
			if is_vertex_grid[mode] then
				mx, my = mx + tile/2, my + tile/2
			end
			local x, y = mx / tile, my / tile
			mx, my = x * tile, y * tile
			if not is_vertex_grid[mode] then
				mx, my = mx + tile/2, my + tile/2
			end
			TR_pos:InplaceSet(mx, my)
			local time = RealTime()

			local orig_print = print
			print = empty_func

			if TR_pos ~= last_pos or (time - DbgTerrainRecoveryViewTime > 500) then
				DbgClear(true)
				last_pos:InplaceSet(TR_pos)
				DbgTerrainRecoveryViewTime = time
				local locks
				if not mz then
					local shift = (tile == 1) and 32 or 16
					local offset = (y << shift) | x
					local dist_to_locks = (restore_dist[mode] or 1) * tile + (buff.radius[offset] or 0)
					local orig = buff.orig[offset]
					if orig then
						
						local minh, maxh = GetMinMaxVoxelHeight(TR_pos)
						TR_box:InplaceSet(mx - tile/2, my - tile/2, minh, mx + tile/2, my + tile/2, maxh)
						TR_box:InplaceGrow(dist_to_locks, dist_to_locks, voxel_height)

						new_objs = {}
						locks = {}
						MapForEach(TR_pos, LockEnumRadius, "ConstructionLock", efConstructionLock | efVisible, nil, gofPermanent, function(obj)
							if TestLock(obj) then
								new_objs[obj] = true
								locks[#locks + 1] = obj
							end
						end)
						MapForEach(TR_pos, max_scavengeable_radius, "ScavengeableObject", efVisible | efAffectsConstruction, nil, gofPermanent, function(obj)
							if TestObstacle(obj) then
								new_objs[obj] = true
								if not meshes[obj] then
									DbgToggleMesh(obj)
								end
							end
						end)
						if mode == "type" or mode == "plants" then
							MapForEach(TR_pos, max_nest_radius, "TerritorialNest", efVisible, function(obj)
								if TestNest(obj) then
									new_objs[obj] = true
									if not obj.dbg_territorial_range then
										obj:AsyncCheatRange()
									end
								end
							end)
						end
					end

					local ok = not next(new_objs)
					local d = Min(100, dist_to_locks / tile)
					for dy = -d,d do
						local yi = y + dy
						for dx = -d,d do
							local xi = x + dx
							local offseti = (yi << shift) | xi
							local origi = buff.orig[offseti]
							if origi then
								local mxi, myi = xi * tile, yi * tile
								if not is_vertex_grid[mode] then
									mxi, myi = mxi + tile/2, myi + tile/2
								end
								if IsCloser2D(mx, my, mxi, myi, dist_to_locks + tile/2) then
									rposi:InplaceSet(mxi, myi)
									local center = dx == 0 and dy == 0
									local color = not center and 0xff444444 or ok and white or red
									if tile == 1 then
										local radius = buff.radius[offseti] or 0
										if radius > 0 then
											DbgAddCircle(rposi, radius, color)
										else
											DbgAddVector(rposi, guim, color)
										end
									else
										DbgAddBox(box(rposi, tile/2 - (center and 0 or 100)), color)
									end
								end
							end
						end
					end
					DbgAddCircle(TR_pos, dist_to_locks + tile/2)
				end
				DbgShowConstructionLocks(locks)
				for obj, mesh in pairs(DbgMeshObjs) do
					if not new_objs or not new_objs[obj] then
						DoneObject(mesh)
						DbgMeshObjs[obj] = nil
					end
				end
				for obj in pairs(DbgTerrainRecoveryViewObjs) do
					if obj.dbg_territorial_range and (not new_objs or not new_objs[obj]) then
						obj:AsyncCheatRange()
					end
				end
			end
			print = orig_print
			DbgTerrainRecoveryViewObjs = new_objs
			WaitWakeup(50)
		end
	end)
end

end -- Platform.debug

----
